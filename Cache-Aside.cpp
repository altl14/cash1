#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>

using namespace std;

unordered_map<string, string> cache;

string filename = "database.txt";

string get_data(string key) {
    if (cache.find(key) != cache.end()) {
        return cache[key];
    }
    else {
        ifstream file(filename);
        string line;
        while (getline(file, line)) {
            if (line.substr(0, key.size()) == key) {
                string data = line.substr(key.size() + 1);
                cache[key] = data;
                return data;
            }
        }
        return "";
    }
}

void add_data_to_db(string key, string data) {
    ifstream file(filename);
    string line;
    bool key_exists = false;
    string new_line = "";
    while (getline(file, line)) {
        if (line.substr(0, key.size()) == key) {
            key_exists = true;
            new_line += key + " " + cache[key] + "\n";
        }
        else {
            new_line += line + "\n";
        }
    }
    if (!key_exists) {
        new_line += key + " " + cache[key] + "\n";
    }
    ofstream outfile;
    outfile.open(filename, ios::trunc);
    outfile << new_line;
}

void add_data(string key, string data) {
    if (cache.find(key) != cache.end()) {
        cache[key] = data;
    }
    else {
        cache[key] = data;
        add_data_to_db(key, data);
    }
}

void update_database() {
    ifstream file(filename);
    string line;
    while (getline(file, line)) {
        string key = line.substr(0, line.find(' '));
        string data = line.substr(line.find(' ') + 1);
        if (cache.find(key) != cache.end() && cache[key] != data) {
            add_data_to_db(key, cache[key]);
        }
    }
}

int main() {
    string key;
    string data;
    string input;

    while (true) {
        getline(cin, input);
        if (input == "add") {
            getline(cin, key);
            getline(cin, data);
            add_data(key, data);
        }
        else if (input == "get") {
            getline(cin, key);
            data = get_data(key);
            cout << "Data for key " << key << ": " << data << '\n';
        }
        else if (input == "Close") {
            break;
        }
        else
            cout << "There is no such a command\n";
    }
    update_database();
    return 0;
}
